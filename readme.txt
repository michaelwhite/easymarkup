
Version: 1.0b4

Requires PHP 5.3+

This library will be a set of classes that parses simple markup languages and generates HTML markup from them.

Currently only bbcode is supported. See test.php for example usage.