<?php

require_once('load-easy-markup.php');

/*
if(file_exists('../php-for-pygments/load-pygments.php')) {
    require_once('../php-for-pygments/load-pygments.php');
}*/

header('Content-Type: text/html');

$bbCode = <<<BBCODE
Making text [i]italic[/i] italic is kind of easy

u is used for the [u]underline[/u] tag

I [s]had been[/s] was born in Denmark

It [size=14px]g[/size][size=18px]r[/size][size=22px]o[/size][size=26px]o[/size][size=28px]w[/size][size=32px]s[/size]!!!

This is some text[center]This is some centered text[/center]

Quoting noone in particular
[quote]'Tis be a bad day[/quote]

Quoting someone in particular
[quote=Bjarne]This be the day of days![/quote]

Linking with no link title
[url]http://www.bbcode.org/[/url]

Linking to a named site
[url=http://www.bbcode.org/]This be bbcode.org![/url]

Including an image
[img]http://www.bbcode.org/images/lubeck_small.jpg[/img]

Resizing the image
[img=100x50]http://www.bbcode.org/images/lubeck_small.jpg[/img]

Making the image clickable (in this case linking to the original image)
[url=http://www.bbcode.org/images/lubeck.jpg][img]http://www.bbcode.org/images/lubeck_small.jpg[/img][/url]

Resizing and adding meta information for the image
[img width="100" height="50" alt="Lubeck city gate" title="This is one of the medieval city gates of Lubeck"]http://www.bbcode.org/images/lubeck_small.jpg[/img]

I would like to [b]emphasize[/b] this

Ordered list
[ol]
[li]Item one[/li]
[li]Item two[/li]
[/ol]

Unordered list
[ul]
[li]Item one[/li]
[li]Item two[/li]
[/ul]

Another variant
[list]
[li]Item one[/li]
[li]Item two[/li]
[/list]

Unordered list
[ul]
[li]Item one[/li]
[*]Item two
* Item three
[/ul]

[table]
[tr]
[th]Name[/th]
[th]Age[/th]
[/tr]
[tr]
[td]John[/td]
[td]65[/td]
[/tr]
[tr]
[td]Gitte[/td]
[td]40[/td]
[/tr]
[tr]
[td]Sussie[/td]
[td]19[/td]
[/tr]
[/table]

[text]
	01  andndnd.
		05  andnd pic x.
		05  andne pic x.
[/text]

[pre]
    This is
      just a
  bunch of
gibberish
[/pre]

About to have css code

[code]
more plain text (but I am in code so I should be line numbered)
[/code]

[code=css]
body {
    color: #333333;
}
[/code]

Done with css code
BBCODE;

#$regex = '~\[b](.+?)\[\/b]~i';
#  ~\[b](.+?)\[\/b]~i
// ~\[b](.+?)\[b]~i
// ~\[b]$1\[b]~i
#echo preg_replace($regex, '<strong>$1</strong>', $bbCode);

$p = new \EasyMarkup\Bbcode();
echo <<<OUTPUT
<!doctype html>
<html>
<head>
<title>Test</title>
<meta charset="utf-8" />
<style type="text/css">
    body {
        font-family: Arial, sans-serif;
        font-size: 12px;
    }

    .bb_p {
        margin-top: 12px;
        margin-bottom: 12px;
    }

    .bb_b {
        font-weight: bold;
    }

    .bb_i {
        font-style: italic;
    }

    .bb_u {
        text-decoration: underline;
    }

    .bb_s {
        text-decoration: line-through;
    }

    .bb_center {
        text-align: center;
    }

    .bb_quote {
        padding: 4px 4px 4px 10px;
    }
</style>
</head>
<body>

OUTPUT;

echo $p->getHtml($bbCode);

echo "</body></html>";