<?php

namespace EasyMarkup;

class Bbcode extends Base {

    protected $_allowComplexTags = true;

    /*
     * TODO - Implement the following tags
     *
     * [email]address[/email]
     * [email=address]Name[/email]
     */

    /** @var array Tokenizer rules for the various bbcode tags. Rules based on http://www.bbcode.org/reference.php */
    protected $_tokenizers = array(
        'br' => array(
            'token' => 'br',
            'tag' => 'br',
            'selfClosing' => true
        ),
        'hr' => array(
            'token' => 'hr',
            'tag' => 'hr',
            'selfClosing' => true
        ),
        'h1' => array(
            'token' => 'h1',
            'tag' => 'h1',
            'complex' => true
        ),
        'h2' => array(
            'token' => 'h2',
            'tag' => 'h2',
            'complex' => true
        ),
        'h3' => array(
            'token' => 'h3',
            'tag' => 'h3',
            'complex' => true
        ),
        'h4' => array(
            'token' => 'h4',
            'tag' => 'h4',
            'complex' => true
        ),
        'h5' => array(
            'token' => 'h5',
            'tag' => 'h5',
            'complex' => true
        ),
        'h6' => array(
            'token' => 'h6',
            'tag' => 'h6',
            'complex' => true
        ),
        'p' => array(
            'token' => 'p',
            'complex' => true,
        ),
        'b' => array(
            'token' => 'b',
            'complex' => true,
        ),
        'i' => array(
            'token' => 'i',
            'complex' => true,
        ),
        'underline' => array(
            'token' => 'u',
            'complex' => true,
        ),
        'line-through' => array(
            'token' => 's',
            'complex' => true,
        ),
        'font-size' => array(
            'regex' => '~\[size=([\da-z]{3,4})\](.+?)\[\/size]~sim',
            'replacement' => '<span class="bb_size" style="font-size: $1;">$2</span>'
        ),
        'font-color' => array(
            'regex' => '~\[color=([#\da-z]*)\](.+?)\[\/color]~sim',
            'replacement' => '<span class="bb_color" style="color: $1;">$2</span>'
        ),
        'center' => array(
            'token' => 'center',
            'tag' => 'div',
            'complex' => true,
        ),
        'quote' => array(
            'token' => 'quote',
            'tag' => 'div',
            'complex' => true,
        ),
        'quote-named' => array(
            'regex' => '~\[quote=(.+?)](.+?)\[\/quote]~sim',
            'replacement' => '<div class="bb_quote bb_namedQuote"><span class="bb_quoteLeadup">$1</span>$2</div>'
        ),
        'link' => array(
            'token' => 'url',
            'replacement' => '<a class="bb_url" href="$1">$1</a>'
        ),
        'link-named' => array(
            'regex' => '~\[url=(.+?)](.+?)\[\/url]~sim',
            'replacement' => '<a class="bb_url bb_namedUrl" href="$1">$2</a>'
        ),
        'image' => array(
            'token' => 'img',
            'replacement' => '<img alt="$1" src="$1" />'
        ),
        'image-resized-shorthand' => array(
            'regex' => '~\[img=([\d]*?)x([\d]*?)](.+?)\[\/img]~sim',
            'replacement' => '<img width="$1" height="$2" alt="$3" src="$3" />'
        ),
        'image-resized-full' => array(
            'regex' => '~\[img (.+?)](.+?)\[\/img]~sim',
            'replacement' => '<img $1 src="$2" />'
        ),
        'list' => array(
            'token' => 'list',
            'tokenClass' => 'ul',
            'tag' => 'ul',
            'complex' => true,
        ),
        'list-ul' => array(
            'token' => 'ul',
            'tag' => 'ul',
            'complex' => true,
        ),
        'list-ol' => array(
            'token' => 'ol',
            'tag' => 'ol',
            'complex' => true,
        ),
        'list-li' => array(
            'token' => 'li',
            'tag' => 'li',
            'complex' => true,
        ),
        'pre' => array(
            'token' => 'pre',
            'tag' => 'pre'
        ),
        'text' => array(
            'token' => 'text',
            'tag' => 'pre',
            'class' => 'pre'
        ),
        'table' => array(
            'token' => 'table',
            'tag' => 'table',
            'complex' => true,
        ),
        'table-row' => array(
            'token' => 'tr',
            'tag' => 'tr',
            'complex' => true,
        ),
        'table-header' => array(
            'token' => 'th',
            'tag' => 'th',
            'complex' => true,
        ),
        'table-cell' => array(
            'token' => 'td',
            'tag' => 'td',
            'complex' => true,
        ),
        'code' => array(
            'regex' => '~\[code(=.+?)?](.+?)\[\/code]~sim',
            'callback' => 'highlightCode'
        ),
        'icode' => array( // in-line code snippet. Usually these are VERY short, just a few words at most.
            'token' => 'icode',
            'tag' => 'pre',
            'complex' => true,
        ),
        'ic' => array( // Short-code for inline-code snippet icode
            'token' => 'ic',
            'tokenClass' => 'icode',
            'tag' => 'pre',
            'complex' => true
        ),
        'note' => array(
            'token' => 'note',
            'tag' => 'span',
            'complex' => true
        )
    );

    protected function _convertWhitespace($bbCode) {
        // Add paragraph and break tags
        $targets = array(
            "\r\n",
            "\r",
            "\n\n"
        );

        $nl = "\n";

        $targetReplacements = array(
            "\n",
            "\n",
            $nl.'</div>'.$nl.$nl.'<div class="bb_p">'.$nl
        );

        return $nl.'<div class="bb_p">'.$nl.str_replace($targets, $targetReplacements, trim($bbCode)).$nl.'</div>'.$nl;
    }

    protected function _obfuscateEscapedTags($bbCode) {
        $bbCode = str_replace(['\['], ['#@!.!@#'], $bbCode);
        #echo $bbCode;
        #exit;
        return $bbCode;
    }

    protected function _deObfuscateEscapedTags($bbCode) {
        $bbCode = str_replace(['#@!.!@#'], ['['], $bbCode);
        return $bbCode;
    }

    /**
     *
     * NOTE: Currently this method looks for opening tags that match certain criteria.
     *
     * TODO: Fix this system to set the bb_<class> to the correct value for bbtags that have a different class name from their tag name.
     *      This will be easy enough to do by using bb_<CLASSNAME> as the target for the replacement and having a secondary process actually replace <CLASSNAME> with that tag's official class name
     *
     * @param $bbCode
     * @return mixed
     */
    protected function _addBbClassesToComplexTags($bbCode) {
        // Find tags that already have a class attribute specified and inject the tag class into it
        $regex = '~\[(\w+)(([^\]])+?)class=[\'"](.+?)[\'"](([^\]])+?)?]~i';
        $replacement = '[$1$2class="bb_$1 $4"$5]';
        $bbCode = preg_replace($regex, $replacement, $bbCode);

        // Now find all the tags that are complex but do not have class attributes and give them the appropriate class
        $regex = '~(\[(\w+)\s)((?![^\[\]]*class=["\'])[^\[\]]+])~i';
        $replacement = '$1class="bb_$2" $3';
        return preg_replace($regex, $replacement, $bbCode);
    }

    protected function _generateRegexes($tokenizer) {

        // Is this tag a self-closing tag?
        if($tokenizer['selfClosing']) {
            if(!$tokenizer['regex']) {
                // Only match on simple tags (without custom attributes of any kind)
                $tokenizer['regex'] = '~\['.$tokenizer['token'].']~i';
            }

            // The complex flag will always be false if complex tags are set as not allowed.
            if($tokenizer['complex'] && !$tokenizer['complexRegex']) {
                // Match on complex tags with zero or more custom attributes defined
                $tokenizer['complexRegex'] = '~\[(('.$tokenizer['token'].')((])|((.+?))(])))~i';
            }
        } else {
            if(!$tokenizer['regex']) {
                // Only match on simple tags (without custom attributes of any kind)
                $tokenizer['regex'] = '~\['.$tokenizer['token'].'](.+?)\[\/'.$tokenizer['token'].']~sim';
            }

            // The complex flag will always be false if complex tags are set as not allowed.
            if($tokenizer['complex'] && !$tokenizer['complexRegex']) {
                // Match on complex tags with zero or more custom attributes defined
                $tokenizer['complexRegex'] = '~\[(('.$tokenizer['token'].')((])|((.+?))(])))(.+?)\[(/'.$tokenizer['token'].')]~sim';
            }
        }

        return $tokenizer;
    }

    protected function _generateReplacements($tokenizer) {

        // Is this tag a self-closing tag?
        if($tokenizer['selfClosing']) {
            if(!$tokenizer['replacement']) {
                // Only match on simple tags (without custom attributes of any kind)
                $tokenizer['replacement'] = '<'.$tokenizer['tag'].' class="bb_'.$tokenizer['tokenClass'].'" />';
            }

            // The complex flag will always be false if complex tags are set as not allowed.
            if($tokenizer['complex'] && !$tokenizer['complexReplacement']) {
                // Match on complex tags with zero or more custom attributes defined
                $tokenizer['complexReplacement'] = '<$2$6 />';
            }
        } else {
            if(!$tokenizer['replacement']) {
                // Replacement for simple tags only.
                $tokenizer['replacement'] = '<'.$tokenizer['tag'].' class="bb_'.$tokenizer['tokenClass'].'">$1</'.$tokenizer['tag'].'>';
            }

            // The complex flag will always be false if complex tags are set as not allowed.
            if($tokenizer['complex']) {
                // Replacement for when the complex tag option is used.
                $tokenizer['complexReplacement'] = '<$2$6>$8<$9>';
            }
        }

        return $tokenizer;
    }

    protected function _normalizeTokenizer($tokenizer) {
        $allOptions = array(
            'token' => false,
            'tokenClass' => false,
            'tag' => 'span',
            'complex' => false,
            'regex' => false,
            'replacement' => false,
            'callback' => false,
            'complexRegex' => false,
            'complexReplacement' => false,
            'selfClosing' => false,
        );

        $tokenizer = array_merge($allOptions, $tokenizer);

        if(!$this->_allowComplexTags) {
            $tokenizer['complex'] = false;
        }

        if(!$tokenizer['tokenClass']) {
            $tokenizer['tokenClass'] = $tokenizer['token'];
        }

        $tokenizer = $this->_generateRegexes($tokenizer);
        $tokenizer = $this->_generateReplacements($tokenizer);

        return $tokenizer;
    }

    public function addTokenRules($rules) {
        $this->_tokenizers = array_merge($this->_tokenizers, $rules);
    }

    public function addTokenRule($ruleName, $ruleConditions) {
        $this->_tokenizers[$ruleName] = $ruleConditions;
    }

    public function getHtml($bbCode) {
        $regexes = array();
        $replacements = array();

        // Reverse-sort the tokenizer keys to ensure the more specific regexes match the content first, then the shorter and potentially less specific regexes can have a shot.
        krsort($this->_tokenizers);

        $callbackTokenizers = array();

        $bbCode = $this->_obfuscateEscapedTags($bbCode);

        if($this->_allowComplexTags) {
            $bbCode = $this->_addBbClassesToComplexTags($bbCode);
        }

        foreach($this->_tokenizers as $tokenizer) {

            $tokenizer = $this->_normalizeTokenizer($tokenizer);

            /* We want to run all of these *after* the entries that do not use callbacks so we copy them to a separate
             * list and skip the rest of this loop. */
            if($tokenizer['callback']) {
                $callbackTokenizers[] = $tokenizer;
                continue;
            }

            $regexes[] = $tokenizer['regex'];
            $replacements[] = $tokenizer['replacement'];

            if($tokenizer['complex']) {
                $regexes[] = $tokenizer['complexRegex'];
                $replacements[] = $tokenizer['complexReplacement'];
            }
        }

        $bbCode = preg_replace($regexes, $replacements, $bbCode);

        // Now run the tokenizers that have callbacks assigned
        foreach($callbackTokenizers as $tokenizer) {
            $bbCode = preg_replace_callback($tokenizer['regex'], array($this, 'callback_'.$tokenizer['callback']), $bbCode);
        }

        $bbCode = $this->_convertWhitespace($bbCode);

        $bbCode = $this->_deObfuscateEscapedTags($bbCode);

        return $bbCode;
    }

    /**
     * Cannot be protected method because it is called from a call to preg_replace_callback()
     *
     * @param $regexMatches
     * @return string
     */
    public function callback_highlightCode($regexMatches) {
        $lexer = trim($regexMatches[1], " \n=") ?: 'text';
        $code = $regexMatches[2];


        // Uses pygments to highlight the code
        if(class_exists('\Pygments', true)) {
            // Get an instance of the Pygments class.
            $pygments = new \Pygments();

            // Set a couple of options.
            $pygments->setOptions(array(
                'linenos' => 'table',   // Render line numbers in a table cell.
                'linespans' => 'code'   // Will not be available unless the server running this script has Pygments 1.6+ installed.
            ));

            return $pygments->highlight($code, $lexer, 'html');
        } else {
            // If pygments is not available, it just returns the code in a pre tag with bb_code as the class.
            return '<pre class="bb_code bb_'.str_replace(array('#', '-'), '_', $lexer).'Code">'.$code.'</pre>';
        }
    }
}