<?php

require_once('load-easy-markup.php');

$formData = $_REQUEST;

$language = @$formData['language'] ?: 'plaintext';
$markup = @$formData['markup'] ?: '';

$output = '';
switch($language) {
    case 'bbcode':
        if($markup) {
            $bbCode = new \EasyMarkup\Bbcode();
            $output = $bbCode->getHtml(htmlentities($markup, ENT_NOQUOTES));
        }
        break;
    case 'plaintext':
    default:
        $output = $markup;
        break;
}

$message = array(
    'result' => $output
);

// Encode json data...
$jsonData = json_encode($message);

// Found some cases where json_encode() does not process the data properly when there are multi-byte or other non-ASCII characters in the content.
// Checking for and handling this case costs about 0.0003 seconds per JSON render when the content is fairly long.
$decodeCheck = json_decode($jsonData);
if((function_exists('mb_strlen') && (mb_strlen($decodeCheck->result) != mb_strlen($message['result']))) || (strlen($decodeCheck->result) != strlen($message['result']))) {
    $message['result'] = 'Encoding error on server. Currently this system expects ASCII or UTF-8 characters.';
    $jsonData = json_encode($message);
}

header('Content-type: application/json');
echo $jsonData;
exit;
